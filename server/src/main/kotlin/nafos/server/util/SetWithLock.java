package nafos.server.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @ClassName SetWithLock
 * @Description TODO
 * @Author hxy
 * @Date 2020/3/16 16:45
 */
public class SetWithLock<T> extends ObjWithLock<Set<T>>  {
    private static final long serialVersionUID = -2305909960649321346L;
    private static final Logger log = LoggerFactory.getLogger(SetWithLock.class);

    public SetWithLock(Set<T> set) {
        super(set);
    }

    public SetWithLock(Set<T> set, ReentrantReadWriteLock lock) {
        super(set, lock);
    }

    public boolean add(T t) {
        ReentrantReadWriteLock.WriteLock writeLock = this.getLock().writeLock();
        writeLock.lock();

        try {
            Set<T> set = (Set)this.getObj();
            boolean var4 = set.add(t);
            return var4;
        } catch (Throwable var8) {
            log.error(var8.getMessage(), var8);
        } finally {
            writeLock.unlock();
        }

        return false;
    }

    public void clear() {
        ReentrantReadWriteLock.WriteLock writeLock = this.getLock().writeLock();
        writeLock.lock();

        try {
            Set<T> set = (Set)this.getObj();
            set.clear();
        } catch (Throwable var6) {
            log.error(var6.getMessage(), var6);
        } finally {
            writeLock.unlock();
        }

    }

    public boolean remove(T t) {
        ReentrantReadWriteLock.WriteLock writeLock = this.getLock().writeLock();
        writeLock.lock();

        try {
            Set<T> set = (Set)this.getObj();
            boolean var4 = set.remove(t);
            return var4;
        } catch (Throwable var8) {
            log.error(var8.getMessage(), var8);
        } finally {
            writeLock.unlock();
        }

        return false;
    }

    public int size() {
        ReentrantReadWriteLock.ReadLock readLock = this.getLock().readLock();
        readLock.lock();

        int var3;
        try {
            Set<T> set = (Set)this.getObj();
            var3 = set.size();
        } finally {
            readLock.unlock();
        }

        return var3;
    }
}
